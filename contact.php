<?php

use PHPMailer\PHPMailer\PHPMailer;
$result = "";
$nameErr = $emailErr ="";
if(isset($_POST['submit'])){
  if(isset($_POST['checkbox'])){
    if(isset($_POST['name']) == true && empty($_POST['name']) == false){
      if(preg_match("/^[a-zA-ZáéűőúóüöíÁÉŰŐÚÓÜÖÍ ]*$/",$_POST['name'])){
        if(isset($_POST['email']) == true && empty($_POST['email']) == false){
          if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == true){

            require_once('vendor/autoload.php');

              $mail = new PHPMailer();
              $mail->SetLanguage("hu", 'vendor\phpmailer\phpmailer\language\phpmailer.lang-hu');
              $mail->IsSMTP();
              //$mail->SMTPDebug = 1;
              $mail->Host = 'tls://smtp.gmail.com:587';
              //$mail->Port = 587;
              $mail->SMTPAuth = true;
              $mail->Username = "thunderclick513s";
              $mail->Password = "thunderpass513";
              $mail->setFrom($_POST['email'],$_POST['name']);
              $mail->addReplyTo('pistibalog90@gmail.com','Balog István');
              $mail->AddAddress('thunderclick513s@gmail.com', 'ThunderClick513s');

              $mail->isHTML(true);
              $mail->CharSet = "utf-8";
              $mail->Subject = $_POST['subject'];
              $mail->Body = "<p><strong>Üdv</strong>, leveled érkezett tőle, <i>".$_POST['name']."</i> (".$_POST['email'].") az üzenet: <br>".$_POST['msg']."</p>";

              if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
              } else {
                header("location:contact.php?sent");
              }
          } else {
            $emailErr = "<p class='text-danger'>Kérlek valós emailcímet adj meg!</p>";
          }
        } else {
          $emailErr = "<p class='text-danger'>Emailcím megadása közelező!</p>";
        }
      } else {
        $nameErr = "<p class='text-danger'>Csak betűket használj kérlek!</p>";
      }
    } else {
      $nameErr = "<p class='text-danger'>Név megadása közelező!</p>";
    }
  } else {
        $result = "<p class='text-danger'>Kérlek fogadd el a feltételeket az üzenetküldéshez!</p>";
  }
}
?>

<!DOCTYPE html>
 <html>
 		<head>
 			<title>Email küldése a fejlesztőknek</title>
 			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
 			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	    <link rel="stylesheet" type="text/css" href="contact_css.css">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	  </head>

	<body>
		<!-- Fejléc/Navigációs lista -->
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="fooldal.php">Hobby Guides</a>
        <button class="navbar-toggler" aria-expanded="false" aria-controls="navbarCollapse" aria-label="Toggle navigation" type="button" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="separator">
								<p> | </p>
						</li>
            <li class="nav-item">
              <a class="nav-link" href="fooldal.php">Főoldal</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="horgaszboltok.php">Horgászboltok</a>
            </li>
						<li class="nav-item">
              <a class="nav-link" href="kerekparboltok.php">Kerékpárboltok</a>
            </li>
						<li class="nav-item">
              <a class="nav-link" href="konditermek.php">Konditermek</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gyik.php">GY.I.K.</a>
            </li>
            <li class="separator">
								<p> | </p>
						</li>
            <p class="contactext active"><a href="contact.php" class="contactkep"><i class="far fa-envelope"></i></a><span class="sr-only">(jelenlegi)</span></p>
          </ul>
            <div class="dropdown mr-2 dropleft">
						  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fiók</a>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
						    <a class="dropdown-item" href="register.php">Regisztáció</a>
						    <a class="dropdown-item" href="login.php">Bejelentkezés</a>
						  </div>
						</div>
				</div>
      </nav>
    </header>
     <br>
 		<!-- Fejléc/Navigációs lista vége -->

     <main role="main">
   				<br>
         <hr class="featurette-divider">
         <!-- Content rész -->
      <div class="container fluid">
        <div class="col-12">
          <div class="text-center">
            <h2>Észrevételed van az oldallal kapcsolatban?</h2><br>
            <h4>Küldj egy emailt a fejlesztőknek!</h4>
          </div>
          <form action="" method="post">
            <div class="form-group">
              <label for="exampleInputName1">Név *</label>
              <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Teljes név">
              <small id="emailHelp" class="form-text text-muted"><?php echo $nameErr ?></small>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email cím *</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email cím">
              <small id="emailHelp" class="form-text text-muted"><?php echo $emailErr ?></small>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Üzenet tárgya</label>
              <input type="text" name="subject" class="form-control" id="exampleInputSubject1" placeholder="Tárgy">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Üzenet</label>
              <textarea name="msg" id="textarea" placeholder="  Üzenet..."></textarea>
            </div>
            <p class="text-danger">A *-gal megjelölt részek kitöltése kötelező!</p>
            <div class="form-group form-check">
              <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">
                  <button type="button" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#exampleModalLong">Általános feltételek</button>
              </label>
              <small id="emailHelp" class="form-text text-muted"><?php echo $result ?></small>
            </div>
            <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Küldés</button>
          </form>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Adatvédelmi tájékoztató</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <h3>1. A Tájékoztató célja és hatálya</h3><br>
                <h5>Jelen adatkezelési tájékoztató (a továbbiakban: „Tájékoztató”) célja, hogy meghatározza a <i>ThunderClick Co.</i> (a továbbiakban: „Adatkezelő”) által vezetett nyilvántartások/adatbázisok felhasználásának törvényes rendjét, valamint biztosítsa az adatvédelem alkotmányos elveinek, az információs önrendelkezési jognak és adatbiztonság követelményeinek érvényesülését, valamint, hogy a törvényi szabályozás keretei között személyes adataival mindenki maga rendelkezzen, azok kezelésének körülményeit megismerhesse, illetve megakadályozza a jogosulatlan hozzáférést, az adatok megváltoztatását és jogosulatlan nyilvánosságra hozatalát. Továbbá jelen Tájékoztató tájékoztatásul szolgál az érintetteknek az Adatkezelő adatkezelési gyakorlatának bemutatására.
                A Tájékoztató hatálya kiterjed az Adatkezelő valamennyi szervezeti egységénél folytatott személyes és különleges adatok kezelésére.</h5><br>
                <h3>2. Irányadó jogszabályok</h3><br>
                <h5>Az Európai Parlament és a Tanács (EU) 2016/679 rendelete (2016. április 27.) a természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről (általános adatvédelmi rendelet; a továbbiakban: „GDPR”)
                évi CXII. törvény az információs önrendelkezési jogról és az információszabadságról (a továbbiakban: „Infotv.”)
                évi V. törvény a Polgári Törvénykönyvről (a továbbiakban: „Ptk.”)
                évi CXXX. törvény a polgári perrendtartásról (a továbbiakban: „Pp.”)
                évi CVIII. törvény – az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről (a továbbiakban: „Eker. tv.”);
                évi XLVIII. törvény – a gazdasági reklámtevékenység alapvető feltételeiről és egyes korlátairól (a továbbiakban: „Grt.”).</h5></br>
                <h3>3. Az Adatkezelő adatai</h3><br>
                <h5>Az Adatkezelő aktuális adatai a következők:<br>
                Név: Thunder Click Co. <br>
                Székhely: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                Postai cím: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                Cégjegyzékszám: <b>(fiktív) -</b> <br>
                Adószám: <b>(fiktív) -</b> <br>
                Nyilvántartó bíróság: <b>(fiktív)</b>Budapest Környéki Törvényszék Cégbírósága <br>
                Telefonszám: <b>(fiktív) -</b> <br>
                E-mail cím: thunderclick513s@gmail.com <br>
                Adatvédelmi tisztviselő neve: ThunderClick Co. <br>
                Adatvédelmi tisztviselő e-mail címe: thunderclick513s@gmail.com <br>
                Adatvédelmi tisztviselő telefonszáma: <b>(fiktív) -</b></h5><br>
                <h3>4. A kezelt személyes adatok köre, az adatkezelés célja, időtartama és jogcíme</h3><br>
                <h5>Az Adatkezelő adatkezeléseit az érintettek önkéntes hozzájárulásán vagy törvényi felhatalmazás alapján végzi. Önkéntes hozzájárulás esetén az érintett bármikor kérhet tájékoztatást a kezelt adatok köréről, illetve azok felhasználásának módjáról, továbbá visszavonhatja hozzájárulását, kivéve meghatározott esetekben, amelyekben jogszabályi kötelezés alapján folytatódik az adatkezelés (ilyen esetekben az Adatkezelő tájékoztatást nyújt az adatok további kezeléséről az érintett részére).
                Az adatközlők kötelesek minden megadott adatot legjobb tudásuk szerint, pontosan közölni.
                Amennyiben az adatközlő nem a saját személyes adatait adja meg, úgy az adatközlő kötelessége az érintett hozzájárulásának beszerzése.
                Amennyiben az Adatkezelő adatfeldolgozók, vagy más harmadik személyek felé továbbítják az adatokat, úgy ezekről az Adatkezelő nyilvántartást vezet. Az adattovábbításról szóló feljegyzésnek tartalmaznia kell az adattovábbítás címzettjét, módját, időpontját, valamint a továbbított adatok körét.
                Az Adatkezelő egyes tevékenységeihez tartozó adatkezelések:
                <h4><i>1.Regisztrációhoz szükséges adatok eltárolása adatbázisban</i></h4>
                Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                A kezelt adatok köre: név, e-mail cím, jelszó <br>
                Az adatkezelés célja: regisztráció validálása <br>
                Adattovábbítás: <b>NINCS</b>
                <h4><i>2.Email küldéshez szükséges adatok(email, név) tárolása email formájában</i></h4>
                Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                A kezelt adatok köre: név, e-mail cím <br>
                Az adatkezelés célja: emailküldés validálása <br>
                Adattovábbítás: <b>NINCS</b></h5><br><br>
                <h3>5. Az érintettek jogai, jogorvoslati lehetőségek</h3><br>
                <h5>Az érintettek bármikor tájékoztatást kérhetnek írásban az Adatkezelőtől az általa kezelt személyes adataik kezelésének módjáról, jelezheti törlési vagy módosítási igényét, továbbá visszavonhatja a korábban megadott hozzájárulását a 3. pontban megadott elérhetőségeken.<br>
                Az érintett törlési jogát a jogszabályban kötelezően előírt adatkezelések esetén nem gyakorolhatja.<br>
                <b>A tájékoztatáshoz való jog tartalma:</b> Az érintett igénye alapján az Adatkezelő az érintett részére a személyes adatok kezelésére vonatkozó, a GDPR 13. és 14. cikkében felsorolt információkat, valamint a 15-22. és a 34. cikk szerinti tájékoztatásokat tömör, közérthető formában átadja.<br>
                <b>A hozzáféréshez való jog tartalma:</b> Az érintett megkeresésére az Adatkezelő tájékoztatást nyújt arról, hogy folyamatban van-e rá vonatkozó adatkezelés az Adatkezelőnél. Amennyiben Adatkezelőnél folyamatban van a kérelmezőre vonatkozó adatkezelés, az érintett jogosult hozzáférésre a következők tekintetében:<br>
                1. A rá vonatkozó személyes adatok;<br>
                2. az adatkezelés célja(i);<br>
                3. az érintett személyes adatok kategóriái;<br>
                4. azon személyek, amelyekkel az érintett adatait közölték, vagy közölni fogják;<br>
                5. az adatok tárolásának időtartama;<br>
                6. a helyesbítéshez, törléshez, valamint az adatkezelés korlátozásához való jog;<br>
                7. a bírósághoz, illetve felügyeleti hatósághoz fordulás joga;<br>
                8. a kezelt adatok forrása;<br>
                9. profilalkotás és/vagy automatizált döntéshozatal, illetve ilyen alkalmazásának részletei, gyakorlati hatásai;<br>
                10. a kezelt adatok harmadik ország vagy nemzetközi szervezet részére való átadása.<br><br>
                A fentiek szerinti adatigénylés esetén Adatkezelő az érintett részére kiadja a kérelemnek megfelelő, általa kezelt adatok egy másolati példányát. Külön kérelemre van lehetőség elektronikus úton való kézbesítést kérni az Adatkezelőtől.<br>
                Adatkezelő minden további példányért oldalanként 0,- Ft-os adminisztrációs díjat kér.<br>
                Az igényelt adatok kiadásának határideje az igény átvételétől számított 30 nap.<br>
                <b>A helyesbítéshez való jog:</b> Az érintett kérheti az Adatkezelő által kezelt, rá vonatkozó pontatlan adatok helyesbítését.<br>
                <b>A törléshez való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, úgy az érintett kérésére Adatkezelő a legrövidebb időn belül, de legkésőbb 5 munkanapon belül, törli az érintettre vonatkozó adatokat:<br>
                1. Az adatok jogellenesen (jogszabályi felhatalmazás vagy személyes hozzájárulás nélkül) kerültek kezelésre;<br>
                2. az adatok kezelése szükségtelen az eredeti cél megvalósításához;<br>
                3. az érintett visszavonja hozzájárulását az adatkezeléshez, és az Adatkezelőnek nincs más jogalapja az adatkezelésre;<br>
                4. a kérdéses adatok gyűjtésére információs társadalommal összefüggő szolgáltatások kínálásával kapcsolatban került sor;<br>
                5. a személyes adatokat az Adatkezelőre vonatkozó jogszabályi kötelezettségek teljesítéséhez törölni kell.<br><br>
                Az adatok törlését Adatkezelőnek nem áll módjában elvégezni, ha az adatkezelés az alábbiak bármelyikéhez szükséges a továbbiakban is:<br>
                1. Az Adatkezelőre vonatkozó jogszabályi előírások teljesítéséhez szükséges a további adatkezelés;<br>
                2. a véleménynyilvánításhoz és a tájékozódáshoz való jog gyakorlása céljából szükséges;<br>
                3. közérdekből;<br>
                4. archiválási, tudományos, kutatási vagy statisztikai célból;<br>
                5. jogi igények érvényesítéséhez vagy védéséhez.<br>
                <b>Az adatkezelés korlátozásához való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, Adatkezelő korlátozza az adatkezelést az érintett kérelmére:<br>
                1. Az érintett vitatja a rá vonatkozó adatok pontosságát, ekkor a korlátozás arra az időre vonatkozik, ameddig a kérdéses adatok pontosságának, helyességének felülvizsgálata hitelt érdemlően megtörténik;<br>
                2. az adatkezelés jogellenes, ugyanakkor az érintett kéri a törlés mellőzését, csupán az adatkezelés korlátozását kéri;<br>
                3. az adatkezeléshez már nincs szükség az adatokra, de az érintett kéri azok további tárolását jogi igényei érvényesítéséhez vagy megvédéséhez;<br>
                <br>Amennyiben az Adatkezelő korlátozást vezet be bármely kezelt adatra, úgy a korlátozás időtartama alatt kizárólag akkor, és annyiban kezeli az érintett adatot, amennyiben:<br>
                Az érintett ehhez hozzájárul;<br>
                jogi igények érvényesítéséhez vagy megvédéséhez szükséges;<br>
                más személy jogainak érvényesítéséhez vagy megvédéséhez szükséges;<br>
                közérdek érvényesítéséhez szükséges.<br>
                <b>A visszavonáshoz való jog:</b> Az érintett jogosult az Adatkezelőnek adott hozzájárulását – írásban – bármikor visszavonni. Ilyen igény esetén az Adatkezelő haladéktalanul és véglegesen törli mindazon adatokat, amelyeket az érintettel kapcsolatosan kezelt, és amelyek további tárolását jogszabály nem írja elő, vagy jogos érdekekhez fűződő jogok érvényesítéséhez vagy megvédéséhez nem szükségesek. A hozzájárulás visszavonásáig történt adatkezelés jogosságát a visszavonás nem érinti.<br>
                <b>Az adathordozáshoz való jog:</b> Az érintett jogosult az Adatkezelő által a rá vonatkozó adatok, általánosan használt, számítógépes szoftverrel olvasható formátumban történő továbbítását kérni egy másik adatkezelő részére. A kérést Adatkezelő a lehető legrövidebb időn belül, de legkésőbb 30 napon belül teljesíti.<br>
                <b>Automatizál döntéshozatal és profilalkotás:</b> Az érintettet megilleti a jogosultság, hogy ne legyen alanya kizárólag automatizált adatkezelésen (például profilalkotáson) alapuló olyan döntésnek, amely rá joghatással lenne, vagy egyébként hátrányosan érintené. Nem alkalmazható ezen jogosultság, ha:<br>
                1. az adatkezelés elengedhetetlen az érintett és az Adatkezelő közötti szerződés megkötése vagy teljesítése céljából;<br>
                2. az érintett kifejezetten hozzájárul ilyen eljárás alkalmazásához;<br>
                3. alkalmazását jogszabály engedélyezi;<br>
                4. szükséges jogi igények érvényesítéséhez vagy védéséhez.</h5><br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal">Bezárás</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal vége -->
 			<hr class="featurette-divider">

       <div class="container marketing">

         <!-- Lábrész -->
         <footer class="container">
           <p class="float-right"><a href="#">Vissza az oldal tetejére.</a></p>
           <p>© 2018-2019 Thunder Click Co. · <a href="#exampleModalLong" data-toggle="modal" data-target="#exampleModalLong">Általános feltételek</a> · <a href="contact.php">Email küldése a fejlesztőknek!</a></p>
         </footer>
   			<!-- Modal -->
         <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLongTitle">Adatvédelmi tájékoztató</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
               </div>
               <div class="modal-body">
                   <h3>1. A Tájékoztató célja és hatálya</h3><br>
                   <h5>Jelen adatkezelési tájékoztató (a továbbiakban: „Tájékoztató”) célja, hogy meghatározza a <i>ThunderClick Co.</i> (a továbbiakban: „Adatkezelő”) által vezetett nyilvántartások/adatbázisok felhasználásának törvényes rendjét, valamint biztosítsa az adatvédelem alkotmányos elveinek, az információs önrendelkezési jognak és adatbiztonság követelményeinek érvényesülését, valamint, hogy a törvényi szabályozás keretei között személyes adataival mindenki maga rendelkezzen, azok kezelésének körülményeit megismerhesse, illetve megakadályozza a jogosulatlan hozzáférést, az adatok megváltoztatását és jogosulatlan nyilvánosságra hozatalát. Továbbá jelen Tájékoztató tájékoztatásul szolgál az érintetteknek az Adatkezelő adatkezelési gyakorlatának bemutatására.
                   A Tájékoztató hatálya kiterjed az Adatkezelő valamennyi szervezeti egységénél folytatott személyes és különleges adatok kezelésére.</h5><br>
                   <h3>2. Irányadó jogszabályok</h3><br>
                   <h5>Az Európai Parlament és a Tanács (EU) 2016/679 rendelete (2016. április 27.) a természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről (általános adatvédelmi rendelet; a továbbiakban: „GDPR”)
                   évi CXII. törvény az információs önrendelkezési jogról és az információszabadságról (a továbbiakban: „Infotv.”)
                   évi V. törvény a Polgári Törvénykönyvről (a továbbiakban: „Ptk.”)
                   évi CXXX. törvény a polgári perrendtartásról (a továbbiakban: „Pp.”)
                   évi CVIII. törvény – az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről (a továbbiakban: „Eker. tv.”);
                   évi XLVIII. törvény – a gazdasági reklámtevékenység alapvető feltételeiről és egyes korlátairól (a továbbiakban: „Grt.”).</h5></br>
                   <h3>3. Az Adatkezelő adatai</h3><br>
                   <h5>Az Adatkezelő aktuális adatai a következők:<br>
                   Név: Thunder Click Co. <br>
                   Székhely: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                   Postai cím: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                   Cégjegyzékszám: <b>(fiktív) -</b> <br>
                   Adószám: <b>(fiktív) -</b> <br>
                   Nyilvántartó bíróság: <b>(fiktív)</b>Budapest Környéki Törvényszék Cégbírósága <br>
                   Telefonszám: <b>(fiktív) -</b> <br>
                   E-mail cím: thunderclick513s@gmail.com <br>
                   Adatvédelmi tisztviselő neve: ThunderClick Co. <br>
                   Adatvédelmi tisztviselő e-mail címe: thunderclick513s@gmail.com <br>
                   Adatvédelmi tisztviselő telefonszáma: <b>(fiktív) -</b></h5><br>
                   <h3>4. A kezelt személyes adatok köre, az adatkezelés célja, időtartama és jogcíme</h3><br>
                   <h5>Az Adatkezelő adatkezeléseit az érintettek önkéntes hozzájárulásán vagy törvényi felhatalmazás alapján végzi. Önkéntes hozzájárulás esetén az érintett bármikor kérhet tájékoztatást a kezelt adatok köréről, illetve azok felhasználásának módjáról, továbbá visszavonhatja hozzájárulását, kivéve meghatározott esetekben, amelyekben jogszabályi kötelezés alapján folytatódik az adatkezelés (ilyen esetekben az Adatkezelő tájékoztatást nyújt az adatok további kezeléséről az érintett részére).
                   Az adatközlők kötelesek minden megadott adatot legjobb tudásuk szerint, pontosan közölni.
                   Amennyiben az adatközlő nem a saját személyes adatait adja meg, úgy az adatközlő kötelessége az érintett hozzájárulásának beszerzése.
                   Amennyiben az Adatkezelő adatfeldolgozók, vagy más harmadik személyek felé továbbítják az adatokat, úgy ezekről az Adatkezelő nyilvántartást vezet. Az adattovábbításról szóló feljegyzésnek tartalmaznia kell az adattovábbítás címzettjét, módját, időpontját, valamint a továbbított adatok körét.
                   Az Adatkezelő egyes tevékenységeihez tartozó adatkezelések:
                   <h4><i>1.Regisztrációhoz szükséges adatok eltárolása adatbázisban</i></h4>
                   Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                   A kezelt adatok köre: név, e-mail cím, jelszó <br>
                   Az adatkezelés célja: regisztráció validálása <br>
                   Adattovábbítás: <b>NINCS</b>
                   <h4><i>2.Email küldéshez szükséges adatok(email, név) tárolása email formájában</i></h4>
                   Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                   A kezelt adatok köre: név, e-mail cím <br>
                   Az adatkezelés célja: emailküldés validálása <br>
                   Adattovábbítás: <b>NINCS</b></h5><br><br>
                   <h3>5. Az érintettek jogai, jogorvoslati lehetőségek</h3><br>
                   <h5>Az érintettek bármikor tájékoztatást kérhetnek írásban az Adatkezelőtől az általa kezelt személyes adataik kezelésének módjáról, jelezheti törlési vagy módosítási igényét, továbbá visszavonhatja a korábban megadott hozzájárulását a 3. pontban megadott elérhetőségeken.<br>
                   Az érintett törlési jogát a jogszabályban kötelezően előírt adatkezelések esetén nem gyakorolhatja.<br>
                   <b>A tájékoztatáshoz való jog tartalma:</b> Az érintett igénye alapján az Adatkezelő az érintett részére a személyes adatok kezelésére vonatkozó, a GDPR 13. és 14. cikkében felsorolt információkat, valamint a 15-22. és a 34. cikk szerinti tájékoztatásokat tömör, közérthető formában átadja.<br>
                   <b>A hozzáféréshez való jog tartalma:</b> Az érintett megkeresésére az Adatkezelő tájékoztatást nyújt arról, hogy folyamatban van-e rá vonatkozó adatkezelés az Adatkezelőnél. Amennyiben Adatkezelőnél folyamatban van a kérelmezőre vonatkozó adatkezelés, az érintett jogosult hozzáférésre a következők tekintetében:<br>
                   1. A rá vonatkozó személyes adatok;<br>
                   2. az adatkezelés célja(i);<br>
                   3. az érintett személyes adatok kategóriái;<br>
                   4. azon személyek, amelyekkel az érintett adatait közölték, vagy közölni fogják;<br>
                   5. az adatok tárolásának időtartama;<br>
                   6. a helyesbítéshez, törléshez, valamint az adatkezelés korlátozásához való jog;<br>
                   7. a bírósághoz, illetve felügyeleti hatósághoz fordulás joga;<br>
                   8. a kezelt adatok forrása;<br>
                   9. profilalkotás és/vagy automatizált döntéshozatal, illetve ilyen alkalmazásának részletei, gyakorlati hatásai;<br>
                   10. a kezelt adatok harmadik ország vagy nemzetközi szervezet részére való átadása.<br><br>
                   A fentiek szerinti adatigénylés esetén Adatkezelő az érintett részére kiadja a kérelemnek megfelelő, általa kezelt adatok egy másolati példányát. Külön kérelemre van lehetőség elektronikus úton való kézbesítést kérni az Adatkezelőtől.<br>
                   Adatkezelő minden további példányért oldalanként 0,- Ft-os adminisztrációs díjat kér.<br>
                   Az igényelt adatok kiadásának határideje az igény átvételétől számított 30 nap.<br>
                   <b>A helyesbítéshez való jog:</b> Az érintett kérheti az Adatkezelő által kezelt, rá vonatkozó pontatlan adatok helyesbítését.<br>
                   <b>A törléshez való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, úgy az érintett kérésére Adatkezelő a legrövidebb időn belül, de legkésőbb 5 munkanapon belül, törli az érintettre vonatkozó adatokat:<br>
                   1. Az adatok jogellenesen (jogszabályi felhatalmazás vagy személyes hozzájárulás nélkül) kerültek kezelésre;<br>
                   2. az adatok kezelése szükségtelen az eredeti cél megvalósításához;<br>
                   3. az érintett visszavonja hozzájárulását az adatkezeléshez, és az Adatkezelőnek nincs más jogalapja az adatkezelésre;<br>
                   4. a kérdéses adatok gyűjtésére információs társadalommal összefüggő szolgáltatások kínálásával kapcsolatban került sor;<br>
                   5. a személyes adatokat az Adatkezelőre vonatkozó jogszabályi kötelezettségek teljesítéséhez törölni kell.<br><br>
                   Az adatok törlését Adatkezelőnek nem áll módjában elvégezni, ha az adatkezelés az alábbiak bármelyikéhez szükséges a továbbiakban is:<br>
                   1. Az Adatkezelőre vonatkozó jogszabályi előírások teljesítéséhez szükséges a további adatkezelés;<br>
                   2. a véleménynyilvánításhoz és a tájékozódáshoz való jog gyakorlása céljából szükséges;<br>
                   3. közérdekből;<br>
                   4. archiválási, tudományos, kutatási vagy statisztikai célból;<br>
                   5. jogi igények érvényesítéséhez vagy védéséhez.<br>
                   <b>Az adatkezelés korlátozásához való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, Adatkezelő korlátozza az adatkezelést az érintett kérelmére:<br>
                   1. Az érintett vitatja a rá vonatkozó adatok pontosságát, ekkor a korlátozás arra az időre vonatkozik, ameddig a kérdéses adatok pontosságának, helyességének felülvizsgálata hitelt érdemlően megtörténik;<br>
                   2. az adatkezelés jogellenes, ugyanakkor az érintett kéri a törlés mellőzését, csupán az adatkezelés korlátozását kéri;<br>
                   3. az adatkezeléshez már nincs szükség az adatokra, de az érintett kéri azok további tárolását jogi igényei érvényesítéséhez vagy megvédéséhez;<br>
                   <br>Amennyiben az Adatkezelő korlátozást vezet be bármely kezelt adatra, úgy a korlátozás időtartama alatt kizárólag akkor, és annyiban kezeli az érintett adatot, amennyiben:<br>
                   Az érintett ehhez hozzájárul;<br>
                   jogi igények érvényesítéséhez vagy megvédéséhez szükséges;<br>
                   más személy jogainak érvényesítéséhez vagy megvédéséhez szükséges;<br>
                   közérdek érvényesítéséhez szükséges.<br>
                   <b>A visszavonáshoz való jog:</b> Az érintett jogosult az Adatkezelőnek adott hozzájárulását – írásban – bármikor visszavonni. Ilyen igény esetén az Adatkezelő haladéktalanul és véglegesen törli mindazon adatokat, amelyeket az érintettel kapcsolatosan kezelt, és amelyek további tárolását jogszabály nem írja elő, vagy jogos érdekekhez fűződő jogok érvényesítéséhez vagy megvédéséhez nem szükségesek. A hozzájárulás visszavonásáig történt adatkezelés jogosságát a visszavonás nem érinti.<br>
                   <b>Az adathordozáshoz való jog:</b> Az érintett jogosult az Adatkezelő által a rá vonatkozó adatok, általánosan használt, számítógépes szoftverrel olvasható formátumban történő továbbítását kérni egy másik adatkezelő részére. A kérést Adatkezelő a lehető legrövidebb időn belül, de legkésőbb 30 napon belül teljesíti.<br>
                   <b>Automatizál döntéshozatal és profilalkotás:</b> Az érintettet megilleti a jogosultság, hogy ne legyen alanya kizárólag automatizált adatkezelésen (például profilalkotáson) alapuló olyan döntésnek, amely rá joghatással lenne, vagy egyébként hátrányosan érintené. Nem alkalmazható ezen jogosultság, ha:<br>
                   1. az adatkezelés elengedhetetlen az érintett és az Adatkezelő közötti szerződés megkötése vagy teljesítése céljából;<br>
                   2. az érintett kifejezetten hozzájárul ilyen eljárás alkalmazásához;<br>
                   3. alkalmazását jogszabály engedélyezi;<br>
                   4. szükséges jogi igények érvényesítéséhez vagy védéséhez.</h5><br>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal">Bezárás</button>
               </div>
             </div>
           </div>
         </div>
         <!-- Modal vége -->
       </main>

     <!-- Javascriptek beágyazása -->
 		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"></script>
 		<script src="../../assets/js/vendor/jquery-slim.min.js"></script>
 		<script src="../../assets/js/vendor/popper.min.js"></script>
 		<script src="../../dist/js/bootstrap.min.js"></script>
 		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
 		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	</body>
 </html>
