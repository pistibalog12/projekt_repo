<?php
$dbname = 'test';
$link = mysqli_connect("localhost","root","") or die("Nem tudtunk kapcsolatot létrehozni a szerverrel.");
$db = mysqli_select_db($link,$dbname) or die("Az adatbázishoz való kapcsolódás sikertelen");

$user_email = mysqli_real_escape_string($link,$_POST['email']);

if (isset($_POST["Submit"]))
{
$md5pass = md5($_POST['pwd']);
$sql = "SELECT id,user_email FROM felhasznalok WHERE
            user_email = '$user_email' AND
            user_pwd = '$md5pass' ";

$result = mysqli_query($link,$sql) or die (mysqli_error($link));
$num = mysqli_num_rows($result);

    if ( $num != 0 ) {


       session_start();
	   list($user_id,$user_email) = mysqli_fetch_row($result);

		$_SESSION['user']= $user_email;


		if (isset($_GET['ret']) && !empty($_GET['ret']))
		{
		header("Location: $_GET[ret]");
		} else
		{
		header("Location: index.php");
		}

		exit();
    }

header("Location: login.php?msg=Hibás adatok");

exit();
}

?>

<!DOCTYPE html>
<html>
		<head>
			<title>Bejelentkezés</title>
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="fooldal_css.css">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta content="text/plain; charset=UTF-8" http-equiv="content-type">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	  </head>

	<body>
		<!-- Fejléc/Navigációs lista -->
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="fooldal.php">Hobby Guides</a>
        <button class="navbar-toggler" aria-expanded="false" aria-controls="navbarCollapse" aria-label="Toggle navigation" type="button" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="separator">
								<p> | </p>
						</li>
            <li class="nav-item">
              <a class="nav-link" href="fooldal.php">Főoldal</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="horgaszboltok.php">Horgászboltok</a>
            </li>
						<li class="nav-item">
              <a class="nav-link" href="kerekparboltok.php">Kerékpárboltok</a>
            </li>
						<li class="nav-item">
              <a class="nav-link" href="konditermek.php">Konditermek</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gyik.php">GY.I.K.</a>
            </li>
            <li class="separator">
								<p> | </p>
						</li>
            <p class="contactext"><a href="contact.php" class="contactkep"><i class="far fa-envelope"></i></a></p>
          </ul>
            <div class="dropdown mr-2 dropleft">
						  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fiók</a>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
						    <a class="dropdown-item" href="register.php">Regisztáció</a>
						    <a class="dropdown-item active" href="login.php">Bejelentkezés<span class="sr-only">(jelenlegi)</span></a>
						  </div>
						</div>
				</div>
      </nav>
    </header>
    <br>
		<!-- Fejléc/Navigációs lista vége -->
    <main role="main">
      <br>
      <!-- Content rész -->
    <?php if (isset($_GET['msg'])) { echo "<div class=\"msg\"> $_GET[msg] </div>"; } ?>
     <div class="container fluid">
       <div class="col-12">
         <div class="text-center">
           <h2>Bejelentkezés</h2><br>
         </div>
         <form name="form1" method="post" action="">
           <div class="form-group">
             <label for="email">E-mail:</label>
             <input type="text" name="email" id="email" class="form-control" placeholder="example@example.com">
           </div>
           <div class="form-group">
             <label for="password">Jelszó:</label>
             <input type="password" name="pwd" id="pwd" class="form-control" placeholder="Jelszó">
           </div>
           <button type="submit" name="Submit" class="btn btn-primary btn-lg btn-block">Belépés</button>
         </form>
       </div>
     </div>

			<hr class="featurette-divider">

      <div class="container marketing">
        <!-- Lábrész -->
        <footer class="container">
          <p class="float-right"><a href="#">Vissza az oldal tetejére.</a></p>
          <p>© 2018-2019 Thunder Click Co. · <a href="#exampleModalLong" data-toggle="modal" data-target="#exampleModalLong">Általános feltételek</a> · <a href="contact.php">Email küldése a fejlesztőknek!</a></p>
        </footer>
  			<!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Adatvédelmi tájékoztató</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <h3>1. A Tájékoztató célja és hatálya</h3><br>
                  <h5>Jelen adatkezelési tájékoztató (a továbbiakban: „Tájékoztató”) célja, hogy meghatározza a <i>ThunderClick Co.</i> (a továbbiakban: „Adatkezelő”) által vezetett nyilvántartások/adatbázisok felhasználásának törvényes rendjét, valamint biztosítsa az adatvédelem alkotmányos elveinek, az információs önrendelkezési jognak és adatbiztonság követelményeinek érvényesülését, valamint, hogy a törvényi szabályozás keretei között személyes adataival mindenki maga rendelkezzen, azok kezelésének körülményeit megismerhesse, illetve megakadályozza a jogosulatlan hozzáférést, az adatok megváltoztatását és jogosulatlan nyilvánosságra hozatalát. Továbbá jelen Tájékoztató tájékoztatásul szolgál az érintetteknek az Adatkezelő adatkezelési gyakorlatának bemutatására.
                  A Tájékoztató hatálya kiterjed az Adatkezelő valamennyi szervezeti egységénél folytatott személyes és különleges adatok kezelésére.</h5><br>
                  <h3>2. Irányadó jogszabályok</h3><br>
                  <h5>Az Európai Parlament és a Tanács (EU) 2016/679 rendelete (2016. április 27.) a természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről (általános adatvédelmi rendelet; a továbbiakban: „GDPR”)
                  évi CXII. törvény az információs önrendelkezési jogról és az információszabadságról (a továbbiakban: „Infotv.”)
                  évi V. törvény a Polgári Törvénykönyvről (a továbbiakban: „Ptk.”)
                  évi CXXX. törvény a polgári perrendtartásról (a továbbiakban: „Pp.”)
                  évi CVIII. törvény – az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről (a továbbiakban: „Eker. tv.”);
                  évi XLVIII. törvény – a gazdasági reklámtevékenység alapvető feltételeiről és egyes korlátairól (a továbbiakban: „Grt.”).</h5></br>
                  <h3>3. Az Adatkezelő adatai</h3><br>
                  <h5>Az Adatkezelő aktuális adatai a következők:<br>
                  Név: Thunder Click Co. <br>
                  Székhely: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                  Postai cím: <b>(fiktív)</b>1191 Budapest, Kossuth tér 12. <br>
                  Cégjegyzékszám: <b>(fiktív) -</b> <br>
                  Adószám: <b>(fiktív) -</b> <br>
                  Nyilvántartó bíróság: <b>(fiktív)</b>Budapest Környéki Törvényszék Cégbírósága <br>
                  Telefonszám: <b>(fiktív) -</b> <br>
                  E-mail cím: thunderclick513s@gmail.com <br>
                  Adatvédelmi tisztviselő neve: ThunderClick Co. <br>
                  Adatvédelmi tisztviselő e-mail címe: thunderclick513s@gmail.com <br>
                  Adatvédelmi tisztviselő telefonszáma: <b>(fiktív) -</b></h5><br>
                  <h3>4. A kezelt személyes adatok köre, az adatkezelés célja, időtartama és jogcíme</h3><br>
                  <h5>Az Adatkezelő adatkezeléseit az érintettek önkéntes hozzájárulásán vagy törvényi felhatalmazás alapján végzi. Önkéntes hozzájárulás esetén az érintett bármikor kérhet tájékoztatást a kezelt adatok köréről, illetve azok felhasználásának módjáról, továbbá visszavonhatja hozzájárulását, kivéve meghatározott esetekben, amelyekben jogszabályi kötelezés alapján folytatódik az adatkezelés (ilyen esetekben az Adatkezelő tájékoztatást nyújt az adatok további kezeléséről az érintett részére).
                  Az adatközlők kötelesek minden megadott adatot legjobb tudásuk szerint, pontosan közölni.
                  Amennyiben az adatközlő nem a saját személyes adatait adja meg, úgy az adatközlő kötelessége az érintett hozzájárulásának beszerzése.
                  Amennyiben az Adatkezelő adatfeldolgozók, vagy más harmadik személyek felé továbbítják az adatokat, úgy ezekről az Adatkezelő nyilvántartást vezet. Az adattovábbításról szóló feljegyzésnek tartalmaznia kell az adattovábbítás címzettjét, módját, időpontját, valamint a továbbított adatok körét.
                  Az Adatkezelő egyes tevékenységeihez tartozó adatkezelések:
                  <h4><i>1.Regisztrációhoz szükséges adatok eltárolása adatbázisban</i></h4>
                  Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                  A kezelt adatok köre: név, e-mail cím, jelszó <br>
                  Az adatkezelés célja: regisztráció validálása <br>
                  Adattovábbítás: <b>NINCS</b>
                  <h4><i>2.Email küldéshez szükséges adatok(email, név) tárolása email formájában</i></h4>
                  Az adatkezelés jogalapja: érintetti hozzájárulás <br>
                  A kezelt adatok köre: név, e-mail cím <br>
                  Az adatkezelés célja: emailküldés validálása <br>
                  Adattovábbítás: <b>NINCS</b></h5><br><br>
                  <h3>5. Az érintettek jogai, jogorvoslati lehetőségek</h3><br>
                  <h5>Az érintettek bármikor tájékoztatást kérhetnek írásban az Adatkezelőtől az általa kezelt személyes adataik kezelésének módjáról, jelezheti törlési vagy módosítási igényét, továbbá visszavonhatja a korábban megadott hozzájárulását a 3. pontban megadott elérhetőségeken.<br>
                  Az érintett törlési jogát a jogszabályban kötelezően előírt adatkezelések esetén nem gyakorolhatja.<br>
                  <b>A tájékoztatáshoz való jog tartalma:</b> Az érintett igénye alapján az Adatkezelő az érintett részére a személyes adatok kezelésére vonatkozó, a GDPR 13. és 14. cikkében felsorolt információkat, valamint a 15-22. és a 34. cikk szerinti tájékoztatásokat tömör, közérthető formában átadja.<br>
                  <b>A hozzáféréshez való jog tartalma:</b> Az érintett megkeresésére az Adatkezelő tájékoztatást nyújt arról, hogy folyamatban van-e rá vonatkozó adatkezelés az Adatkezelőnél. Amennyiben Adatkezelőnél folyamatban van a kérelmezőre vonatkozó adatkezelés, az érintett jogosult hozzáférésre a következők tekintetében:<br>
                  1. A rá vonatkozó személyes adatok;<br>
                  2. az adatkezelés célja(i);<br>
                  3. az érintett személyes adatok kategóriái;<br>
                  4. azon személyek, amelyekkel az érintett adatait közölték, vagy közölni fogják;<br>
                  5. az adatok tárolásának időtartama;<br>
                  6. a helyesbítéshez, törléshez, valamint az adatkezelés korlátozásához való jog;<br>
                  7. a bírósághoz, illetve felügyeleti hatósághoz fordulás joga;<br>
                  8. a kezelt adatok forrása;<br>
                  9. profilalkotás és/vagy automatizált döntéshozatal, illetve ilyen alkalmazásának részletei, gyakorlati hatásai;<br>
                  10. a kezelt adatok harmadik ország vagy nemzetközi szervezet részére való átadása.<br><br>
                  A fentiek szerinti adatigénylés esetén Adatkezelő az érintett részére kiadja a kérelemnek megfelelő, általa kezelt adatok egy másolati példányát. Külön kérelemre van lehetőség elektronikus úton való kézbesítést kérni az Adatkezelőtől.<br>
                  Adatkezelő minden további példányért oldalanként 0,- Ft-os adminisztrációs díjat kér.<br>
                  Az igényelt adatok kiadásának határideje az igény átvételétől számított 30 nap.<br>
                  <b>A helyesbítéshez való jog:</b> Az érintett kérheti az Adatkezelő által kezelt, rá vonatkozó pontatlan adatok helyesbítését.<br>
                  <b>A törléshez való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, úgy az érintett kérésére Adatkezelő a legrövidebb időn belül, de legkésőbb 5 munkanapon belül, törli az érintettre vonatkozó adatokat:<br>
                  1. Az adatok jogellenesen (jogszabályi felhatalmazás vagy személyes hozzájárulás nélkül) kerültek kezelésre;<br>
                  2. az adatok kezelése szükségtelen az eredeti cél megvalósításához;<br>
                  3. az érintett visszavonja hozzájárulását az adatkezeléshez, és az Adatkezelőnek nincs más jogalapja az adatkezelésre;<br>
                  4. a kérdéses adatok gyűjtésére információs társadalommal összefüggő szolgáltatások kínálásával kapcsolatban került sor;<br>
                  5. a személyes adatokat az Adatkezelőre vonatkozó jogszabályi kötelezettségek teljesítéséhez törölni kell.<br><br>
                  Az adatok törlését Adatkezelőnek nem áll módjában elvégezni, ha az adatkezelés az alábbiak bármelyikéhez szükséges a továbbiakban is:<br>
                  1. Az Adatkezelőre vonatkozó jogszabályi előírások teljesítéséhez szükséges a további adatkezelés;<br>
                  2. a véleménynyilvánításhoz és a tájékozódáshoz való jog gyakorlása céljából szükséges;<br>
                  3. közérdekből;<br>
                  4. archiválási, tudományos, kutatási vagy statisztikai célból;<br>
                  5. jogi igények érvényesítéséhez vagy védéséhez.<br>
                  <b>Az adatkezelés korlátozásához való jog:</b> Amennyiben az alábbi indokok bármelyike fennáll, Adatkezelő korlátozza az adatkezelést az érintett kérelmére:<br>
                  1. Az érintett vitatja a rá vonatkozó adatok pontosságát, ekkor a korlátozás arra az időre vonatkozik, ameddig a kérdéses adatok pontosságának, helyességének felülvizsgálata hitelt érdemlően megtörténik;<br>
                  2. az adatkezelés jogellenes, ugyanakkor az érintett kéri a törlés mellőzését, csupán az adatkezelés korlátozását kéri;<br>
                  3. az adatkezeléshez már nincs szükség az adatokra, de az érintett kéri azok további tárolását jogi igényei érvényesítéséhez vagy megvédéséhez;<br>
                  <br>Amennyiben az Adatkezelő korlátozást vezet be bármely kezelt adatra, úgy a korlátozás időtartama alatt kizárólag akkor, és annyiban kezeli az érintett adatot, amennyiben:<br>
                  Az érintett ehhez hozzájárul;<br>
                  jogi igények érvényesítéséhez vagy megvédéséhez szükséges;<br>
                  más személy jogainak érvényesítéséhez vagy megvédéséhez szükséges;<br>
                  közérdek érvényesítéséhez szükséges.<br>
                  <b>A visszavonáshoz való jog:</b> Az érintett jogosult az Adatkezelőnek adott hozzájárulását – írásban – bármikor visszavonni. Ilyen igény esetén az Adatkezelő haladéktalanul és véglegesen törli mindazon adatokat, amelyeket az érintettel kapcsolatosan kezelt, és amelyek további tárolását jogszabály nem írja elő, vagy jogos érdekekhez fűződő jogok érvényesítéséhez vagy megvédéséhez nem szükségesek. A hozzájárulás visszavonásáig történt adatkezelés jogosságát a visszavonás nem érinti.<br>
                  <b>Az adathordozáshoz való jog:</b> Az érintett jogosult az Adatkezelő által a rá vonatkozó adatok, általánosan használt, számítógépes szoftverrel olvasható formátumban történő továbbítását kérni egy másik adatkezelő részére. A kérést Adatkezelő a lehető legrövidebb időn belül, de legkésőbb 30 napon belül teljesíti.<br>
                  <b>Automatizál döntéshozatal és profilalkotás:</b> Az érintettet megilleti a jogosultság, hogy ne legyen alanya kizárólag automatizált adatkezelésen (például profilalkotáson) alapuló olyan döntésnek, amely rá joghatással lenne, vagy egyébként hátrányosan érintené. Nem alkalmazható ezen jogosultság, ha:<br>
                  1. az adatkezelés elengedhetetlen az érintett és az Adatkezelő közötti szerződés megkötése vagy teljesítése céljából;<br>
                  2. az érintett kifejezetten hozzájárul ilyen eljárás alkalmazásához;<br>
                  3. alkalmazását jogszabály engedélyezi;<br>
                  4. szükséges jogi igények érvényesítéséhez vagy védéséhez.</h5><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal">Bezárás</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal vége -->
      </main>

    <!-- Javascriptek beágyazása -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"></script>
		<script src="../../assets/js/vendor/jquery-slim.min.js"></script>
		<script src="../../assets/js/vendor/popper.min.js"></script>
		<script src="../../dist/js/bootstrap.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	</body>
</html>
